package Model;

import Model.MyImage;
import org.junit.Test;

import java.awt.image.BufferedImage;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

public class MyImageTests {
    MyImage testImage;

    @Test (expected = NullPointerException.class)
    public void nullMyImageIsEmpty_willThrowNullPointerException() throws Exception {
        testImage = null;

        testImage.isEmpty();
    }

    @Test
    public void newMyImageIsNotEmpty() throws Exception {
        testImage = new MyImage(new BufferedImage(5, 5, BufferedImage.TYPE_INT_RGB));
        assertFalse(testImage.isEmpty());
    }

    @Test (expected = NullPointerException.class)
    public void nullMyImageGetSize_willThrowNullPointerException() throws Exception {
        testImage = null;

        testImage.getImageSize();
    }

    @Test
    public void newMyImageGetSizeIsCorrect() throws Exception {
        int height = 12;
        int width = 13;

        testImage = new MyImage(new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB));

        assertEquals(width,testImage.getImageSize().getWidth());
        assertEquals(height,testImage.getImageSize().getHeight());
    }
}
