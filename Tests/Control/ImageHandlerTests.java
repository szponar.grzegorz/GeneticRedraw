package Control;

import Model.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class ImageHandlerTests {
    MyImage testImage;
    RoundingMode rm = RoundingMode.HALF_UP;

    private void assertColorsAreEqual(Pixel pixelActual, Pixel pixel) {
        assertEquals(pixelActual.getColor().getRed(), pixel.getColor().getRed());
        assertEquals(pixelActual.getColor().getGreen(), pixel.getColor().getGreen());
        assertEquals(pixelActual.getColor().getBlue(), pixel.getColor().getBlue());
        assertEquals(pixelActual.getColor().getAlpha(), pixel.getColor().getAlpha());
    }

    @Before
    public void setUp() throws Exception {
        testImage = ImageIOHandler.createNewImage(new Size(100, 100));
    }

    @Test
    public void drawPointSetsColorCorrectly() throws Exception {
        testImage = ImageIOHandler.createNewImage(new Size(1, 1));
        Pixel pixel = new Pixel(new Coordinate(0, 0), new Color(125, 125, 125, 255));
        ImageHandler.drawPixel(testImage, pixel);
        Pixel[] pixelArray = ImageHandler.getImageIntoPixelArray(testImage);

        assertColorsAreEqual(pixel, pixelArray[0]);
    }

    @Test
    public void getImageIntoPixelArrayCreatesCorrectRgbValue() throws Exception {
        testImage = ImageIOHandler.createNewImage(new Size(5, 1));
        Pixel pixel0 = new Pixel(new Coordinate(0,0), new Color(0,0,255,255));
        Pixel pixel1 = new Pixel(new Coordinate(1,0), new Color(0,255,0,255));
        Pixel pixel2 = new Pixel(new Coordinate(2,0), new Color(255,0,0,255));
        Pixel pixel3 = new Pixel(new Coordinate(3,0), new Color(0,0,0,255));
        Pixel pixel4 = new Pixel(new Coordinate(4,0), new Color(255,255,255,255));
        ImageHandler.drawPixel(testImage, pixel0);
        ImageHandler.drawPixel(testImage, pixel1);
        ImageHandler.drawPixel(testImage, pixel2);
        ImageHandler.drawPixel(testImage, pixel3);
        ImageHandler.drawPixel(testImage, pixel4);
        Pixel[] pixelArray;
        pixelArray = ImageHandler.getImageIntoPixelArray(testImage);

        assertColorsAreEqual(pixel0, pixelArray[0]);
        assertColorsAreEqual(pixel1, pixelArray[1]);
        assertColorsAreEqual(pixel2, pixelArray[2]);
        assertColorsAreEqual(pixel3, pixelArray[3]);
        assertColorsAreEqual(pixel4, pixelArray[4]);
    }


    @Test
    public void getImagesSimilarity_ProvideOneHundredPercentage() throws Exception {
        testImage = ImageIOHandler.createNewImage(new Size(1, 1));
        MyImage outputImage1 = ImageIOHandler.createNewImage(new Size(1, 1));

        ImageHandler.drawPixel(testImage, new Pixel(new Coordinate(0, 0), new Color(0, 0, 0, 255)));
        ImageHandler.drawPixel(outputImage1, new Pixel(new Coordinate(0, 0), new Color(0, 0, 0, 255)));

        System.out.println(ImageHandler.getImagesRoundedSimilarity(testImage, outputImage1)); // TODO only for debugging
        assertEquals(new BigDecimal(100.00).setScale(2,rm), ImageHandler.getImagesRoundedSimilarity(testImage, outputImage1));
    }

    @Test
    public void getImagesSimilarity_ProvideZeroPercentage() throws Exception {
        testImage = ImageIOHandler.createNewImage(new Size(1, 1));
        MyImage outputImage2 = ImageIOHandler.createNewImage(new Size(1, 1));

        ImageHandler.drawPixel(testImage, new Pixel(new Coordinate(0, 0), new Color(0, 0, 0, 255)));
        ImageHandler.drawPixel(outputImage2, new Pixel(new Coordinate(0, 0), new Color(255, 255, 255, 255)));

        assertEquals(new BigDecimal(0).setScale(2,rm), ImageHandler.getImagesRoundedSimilarity(testImage, outputImage2));
    }

    @Test //TODO
    public void getImagesSimilarity_ProvideAroundFiftyPercentage() throws Exception {
        testImage = ImageIOHandler.createNewImage(new Size(1, 1));
        MyImage outputImage3 = ImageIOHandler.createNewImage(new Size(1, 1));

        ImageHandler.drawPixel(testImage, new Pixel(new Coordinate(0, 0), new Color(0, 0, 0, 253)));
        ImageHandler.drawPixel(outputImage3, new Pixel(new Coordinate(0, 0), new Color(127, 127, 127, 127)));

        System.out.println(ImageHandler.getImagesRoundedSimilarity(testImage, outputImage3)); // TODO only for debugging
        assertEquals(new BigDecimal(50.20).setScale(2,rm), ImageHandler.getImagesRoundedSimilarity(testImage, outputImage3));
    }

    @Test //TODO
    public void getImagesSimilarity_ProvideAroundTwentyFivePercentage() throws Exception {
        testImage = ImageIOHandler.createNewImage(new Size(1, 1));
        MyImage outputImage4 = ImageIOHandler.createNewImage(new Size(1, 1));

        ImageHandler.drawPixel(testImage, new Pixel(new Coordinate(0, 0), new Color(0, 0, 0, 255)));
        ImageHandler.drawPixel(outputImage4, new Pixel(new Coordinate(0, 0), new Color(192, 192, 192, 255)));

        System.out.println(ImageHandler.getImagesRoundedSimilarity(testImage, outputImage4)); // TODO only for debugging
        assertEquals(new BigDecimal(24.71).setScale(2,rm), ImageHandler.getImagesRoundedSimilarity(testImage, outputImage4));
    }
}
