package Control;

import Model.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import javax.imageio.IIOException;
import java.io.FileNotFoundException;
import java.io.IOException;
import Exception.WrongFileException;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertFalse;

public class ImageIOHandlerTests {
    MyImage testImage;

    @Before
    public void setUp() throws Exception {
        testImage = new MyImage();
    }

    @Test
    public void checkIfCreateNewImageIsNotEmpty() throws Exception {
        Size size = new Size(50,50);
        testImage = ImageIOHandler.createNewImage(size);

        assertFalse(testImage.isEmpty());
    }

    @Test
     public void checkIfGetNewImageIsNotEmpty() throws Exception {
        String filename = "test.jpg";
        testImage = ImageIOHandler.getImageFromFile(filename);

        assertFalse(testImage.isEmpty());
    }

    @Test
    public void writeImageCreatesFile() throws Exception {
        String outputName = "writeImageTest.png";
        MyImage testMyImage = ImageIOHandler.createNewImage(new Size(10, 10));
        ImageIOHandler.writeImage(testMyImage, outputName);
        testImage = ImageIOHandler.getImageFromFile(outputName);

        assertFalse(testImage.isEmpty());
    }

    @Test (expected = NullPointerException.class)
    public void writeEmptyImage_throwsNullPointerException() throws Exception {
        String outputName = "nullImage.png";
        MyImage nullImage = null;
        ImageIOHandler.writeImage(nullImage, outputName);
    }

    @Test (expected = NullPointerException.class)
    public void writeImageNullName_throwsNullPointerException() throws Exception {
        String outputName = null;
        ImageIOHandler.writeImage(testImage, outputName);
    }

    @Ignore // TODO dunno why it's not working
    @Test (expected = Exception.class)
    public void writeImagePathIsNotExisting_throwsNullPointerException() throws Exception {
        String outputName = "x:\\not\\existing\\test.png";
        testImage = ImageIOHandler.createNewImage(new Size(10, 10));
            ImageIOHandler.writeImage(testImage, outputName);
    }

    @Test (expected = IIOException.class)
    public void getNotExistingImage_throwsIIOException() throws Exception {
        String filename = "doesNotExist.jpg";
        testImage = ImageIOHandler.getImageFromFile(filename);
    }

    @Test (expected = WrongFileException.class)
    public void getImageInTxtExtension_throwsIOException() throws Exception {
        String filename = "test.txt";
        testImage = ImageIOHandler.getImageFromFile(filename);
    }

    @Ignore // TODO dunno why it's not working
    @Test (expected = Exception.class)
    public void getCorruptedImage_throwsIOException() throws Exception {
        String filename = "corrupted.jpg";
        testImage = ImageIOHandler.getImageFromFile(filename);
    }

    @Test (expected = NullPointerException.class)
    public void getNullInsteadImage_throwsNullPointerException() throws Exception {
        String filename = null;
        testImage = ImageIOHandler.getImageFromFile(filename);
    }

    @Test
    public void testGetCreatedImageSize() throws Exception {
        int height = 12;
        int width = 13;

        testImage =  ImageIOHandler.createNewImage(new Size(width, height));
        Size testSize = testImage.getImageSize();

        assertEquals(height,testSize.getHeight());
        assertEquals(width,testSize.getWidth());
    }

    @Test (expected = NullPointerException.class)
    public void testGetNullImageSize() throws Exception {
        MyImage nullImage = null;
        Size testSize = nullImage.getImageSize();
    }


}
