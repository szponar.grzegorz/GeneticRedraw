package Control;

import org.jaga.definitions.GAParameterSet;
import org.jaga.hooks.BetterResultHook;
import org.jaga.individualRepresentation.greycodedNumbers.NDecimalsIndividualSimpleFactory;
import org.jaga.individualRepresentation.greycodedNumbers.RangeConstraint;
import org.jaga.masterAlgorithm.ReusableSimpleGA;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AlgorithmGeneticTests {

    @InjectMocks
    AlgorithmGenetic algorithmGenetic;
    @Mock
    private AlgorithmParameterSetFactory mockedAlgorithmParameterSetFactory;
    @Mock
    private FitnessEvaluationAlgorithmFactory mockedFitnessFactory;
    @Mock
    private SelectionAlgorithmFactory mockedAlgorithmFactory;
    @Mock
    private GAParameterSet mockedGaParameterSet;
    @Mock
    private NDecimalsIndividualSimpleFactory mockedFact;
    @Mock
    private ReusableSimpleGA ga;
    @Mock
    private BetterResultHook hook;

    @Ignore //in-progress
    @Test
    public void onSetParams_masterAlgIsConfiguredCorrectly(){
        when(mockedAlgorithmParameterSetFactory.create()).thenReturn(mockedGaParameterSet);

        algorithmGenetic.setParams();

        verify(mockedGaParameterSet, times(1)).setPopulationSize(100);
        verify(mockedGaParameterSet, times(1)).setFitnessEvaluationAlgorithm(mockedFitnessFactory.create());
        verify(mockedGaParameterSet, times(1)).setSelectionAlgorithm(mockedAlgorithmFactory.create(-10E3));
        verify(mockedGaParameterSet, times(1)).setMaxGenerationNumber(200);
        NDecimalsIndividualSimpleFactory testFact = algorithmGenetic.setIndividualSimpleFactory(2, 0, 10);
        verify(mockedGaParameterSet, times(1)).setIndividualsFactory(testFact);
    }

    @Ignore //in-progress
    @Test
    public void onsetIndividualSimpleFactory_ConstraintAreConfiguredCorrectly(){
        //when(algorithmGenetic.setIndividualSimpleFactory(2, 0, 10)).thenReturn(mockedFact);
        when(mockedAlgorithmParameterSetFactory.create()).thenReturn(mockedGaParameterSet);

        algorithmGenetic.setParams();

        verify(mockedFact, times(1)).setIndividualSize(2);
        verify(mockedFact, times(1)).setDecimalScale(0);
        verify(mockedFact, times(1)).setPrecision(10);
        verify(mockedFact, times(1)).setConstraint(0, new RangeConstraint(1, 100));
        verify(mockedFact, times(1)).setConstraint(1, new RangeConstraint(1, 100));
    }
}
