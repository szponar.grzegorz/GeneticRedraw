package Control;

import org.jaga.definitions.SelectionAlgorithm;

/**
 * Created by d00gy on 22/02/2016.
 */
public interface SelectionAlgorithmFactory {
    SelectionAlgorithm create(double v);
}
