package Control;

import Exception.WrongFileException;
import Model.MyImage;
import Model.Size;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


public class ImageIOHandler {

    public static MyImage getImageFromFile(String filename) throws IOException, WrongFileException {
        MyImage myImage;
        if (fileNameEndsWithExtension(filename, ".jpg") || fileNameEndsWithExtension(filename, ".png"))
            myImage = new MyImage(ImageIO.read(new File(filename)));
        else
            throw new WrongFileException();
        return myImage;
    }

        private static boolean fileNameEndsWithExtension(String filename, String suffix) {
            return filename.toLowerCase().endsWith(suffix);
        }

    public static MyImage createNewImage(Size size) {
        return new MyImage(new BufferedImage(size.getWidth(), size.getHeight(), BufferedImage.TYPE_INT_RGB));
    }

    public static void writeImage(MyImage myImage, String filename) throws IOException {
        ImageIO.write(myImage.getImageCopy(), "png", new File(filename));
    }

}
