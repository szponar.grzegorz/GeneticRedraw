package Control;

import Model.Coordinate;
import Model.MyImage;
import Model.Pixel;
import Model.Size;

import java.awt.image.BufferedImage;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class ImageHandler {

    public static MyImage drawTriangle(MyImage myImage){
        return null;
    }

    public static MyImage drawPixel(MyImage myImage, Pixel myPixel) {
        BufferedImage bufferedImage = myImage.getImageCopy();
        bufferedImage.setRGB(myPixel.getCoordinate().getX(), myPixel.getCoordinate().getY(), myPixel.getColor().getByteColor(myPixel));

        return new MyImage(bufferedImage);
    }

    public static Pixel[] getImageIntoPixelArray(MyImage myImage){
        Size size = myImage.getImageSize();
        int numberOfPixels = size.getHeight()*size.getWidth();
        Pixel[] pixelArray = new Pixel[numberOfPixels];
        int pixelNumber = 0;

        for (int pixelX = 0; pixelX < size.getWidth(); pixelX++){
            for (int pixelY = 0; pixelY < size.getHeight(); pixelY++){
                pixelArray[pixelNumber] = getPixelWithColor(myImage, new Coordinate(pixelX, pixelY));
                pixelNumber++;
            }
        }
        return pixelArray;
    }

        private static Pixel getPixelWithColor(MyImage myImage, Coordinate coordinate) {
            Pixel pixel = new Pixel(coordinate);
            pixel.setByteColor(myImage.getImageCopy().getRGB(coordinate.getX(), coordinate.getY()));

            return pixel;
        }

    public static BigDecimal getImagesRoundedSimilarity(MyImage actualImage, MyImage outputImage){
        Pixel[] actualPixelArray, outputPixelArray;
        BigDecimal[] pixelSimilarity;
        BigDecimal sumOfPixelDifference, maximalDifferenceBetweenImages, similarity;

        maximalDifferenceBetweenImages = getMaximalDifferenceBetweenImages(actualImage);
        actualPixelArray = ImageHandler.getImageIntoPixelArray(actualImage);
        outputPixelArray = ImageHandler.getImageIntoPixelArray(outputImage);

        pixelSimilarity = getArrayOfPixelSimilarity(actualPixelArray, outputPixelArray);
        sumOfPixelDifference = getSumOfPixelDifference(actualPixelArray, pixelSimilarity);
        similarity = (maximalDifferenceBetweenImages.subtract(sumOfPixelDifference)).divide(maximalDifferenceBetweenImages, 3, BigDecimal.ROUND_UP);

        return similarity.movePointRight(2).setScale(2, RoundingMode.HALF_UP);
    }

        private static BigDecimal getMaximalDifferenceBetweenImages(MyImage actualImage) {
            BigDecimal maximalDifferenceBetweenImages = new BigDecimal(actualImage.getImageSize().getHeight()).multiply(
                new BigDecimal(actualImage.getImageSize().getWidth()).multiply(
                        new BigDecimal (Math.sqrt(Math.pow(255, 2) * 3))));

        return maximalDifferenceBetweenImages;
    }

        private static BigDecimal getSumOfPixelDifference(Pixel[] actualPixelArray, BigDecimal[] pixelSimilarity) {
            BigDecimal sumOfPixelDifference = new BigDecimal("0");
            for (int pixelNumber = 0; pixelNumber < actualPixelArray.length; pixelNumber++){
                sumOfPixelDifference = sumOfPixelDifference.add(pixelSimilarity[pixelNumber]);
            }
            sumOfPixelDifference = sumOfPixelDifference.divide(new BigDecimal (actualPixelArray.length), 3, BigDecimal.ROUND_UP);
            return sumOfPixelDifference;
        }

            private static BigDecimal[] getArrayOfPixelSimilarity(Pixel[] actualPixelArray, Pixel[] outputPixelArray) {
                BigDecimal[] pixelSimilarity = new BigDecimal[actualPixelArray.length];
                for (int pixelNumber = 0; pixelNumber < actualPixelArray.length; pixelNumber++){
                    pixelSimilarity[pixelNumber] = getPixelSimilarity(actualPixelArray, outputPixelArray, pixelNumber);
                }
                return pixelSimilarity;
            }

        private static BigDecimal getPixelSimilarity(Pixel[] actualPixelArray, Pixel[] outputPixelArray, int pixelNumber) {
            return sqrt(
                    new BigDecimal(actualPixelArray[pixelNumber].getColor().getRed() - outputPixelArray[pixelNumber].getColor().getRed()).pow(2).add(
                    new BigDecimal(actualPixelArray[pixelNumber].getColor().getGreen() - outputPixelArray[pixelNumber].getColor().getGreen()).pow(2).add(
                    new BigDecimal(actualPixelArray[pixelNumber].getColor().getBlue() - outputPixelArray[pixelNumber].getColor().getBlue()).pow(2))));

            //return Math.sqrt(
            // Math.pow(actualPixelArray[pixelNumber].getColor().getRed() - outputPixelArray[pixelNumber].getColor().getRed(), 2)
            //                + Math.pow(actualPixelArray[pixelNumber].getColor().getGreen() - outputPixelArray[pixelNumber].getColor().getGreen(), 2)
            //                + Math.pow(actualPixelArray[pixelNumber].getColor().getBlue() - outputPixelArray[pixelNumber].getColor().getBlue(), 2));
        }

        private static BigDecimal sqrt(BigDecimal value) {
            BigDecimal x = new BigDecimal(Math.sqrt(value.doubleValue()));
        return x.add(new BigDecimal(value.subtract(x.multiply(x)).doubleValue() / (x.doubleValue() * 2.0)));
    }
}
