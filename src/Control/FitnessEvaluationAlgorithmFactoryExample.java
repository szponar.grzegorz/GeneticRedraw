package Control;

import org.jaga.definitions.FitnessEvaluationAlgorithm;
import org.jaga.exampleApplications.Example2Fitness;

/**
 * Created by d00gy on 22/02/2016.
 */
public class FitnessEvaluationAlgorithmFactoryExample implements FitnessEvaluationAlgorithmFactory{

    public FitnessEvaluationAlgorithm create(){
        return new Example2Fitness();
    }
}
