package Control;

import org.jaga.definitions.FitnessEvaluationAlgorithm;

/**
 * Created by d00gy on 22/02/2016.
 */
public interface FitnessEvaluationAlgorithmFactory {
    FitnessEvaluationAlgorithm create();
}
