package Control;

import org.jaga.definitions.GAParameterSet;

/**
 * Created by gregory.szponar on 16/02/2016.
 */
public interface AlgorithmParameterSetFactory {
    GAParameterSet create();
}
