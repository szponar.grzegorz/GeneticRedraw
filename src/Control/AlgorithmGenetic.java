package Control;

import org.jaga.definitions.GAParameterSet;
import org.jaga.definitions.GAResult;
import org.jaga.hooks.BetterResultHook;
import org.jaga.individualRepresentation.greycodedNumbers.NDecimalsIndividualSimpleFactory;
import org.jaga.individualRepresentation.greycodedNumbers.RangeConstraint;
import org.jaga.masterAlgorithm.ReusableSimpleGA;

public class AlgorithmGenetic implements Algorithm {
    private AlgorithmParameterSetFactory parameterFactory;
    private FitnessEvaluationAlgorithmFactory fitnessFactory;
    private SelectionAlgorithmFactory algorithmFactory;
    private GAParameterSet gaParameterSet;
    private ReusableSimpleGA ga;
    private BetterResultHook hook;

    public AlgorithmGenetic(AlgorithmParameterSetFactory parametersFactory, FitnessEvaluationAlgorithmFactory fitnessFactory, SelectionAlgorithmFactory algorithmFactory) {
        this.parameterFactory = parametersFactory;
        this.fitnessFactory = fitnessFactory;
        this.algorithmFactory = algorithmFactory;
    }

    public void setParams() {

        gaParameterSet = parameterFactory.create();
        gaParameterSet.setPopulationSize(100);
        gaParameterSet.setFitnessEvaluationAlgorithm(fitnessFactory.create());
        gaParameterSet.setSelectionAlgorithm(algorithmFactory.create(-10E3));
        gaParameterSet.setMaxGenerationNumber(200);
        NDecimalsIndividualSimpleFactory fact = setIndividualSimpleFactory(2, 0, 10);
        gaParameterSet.setIndividualsFactory(fact);
    }

        protected NDecimalsIndividualSimpleFactory setIndividualSimpleFactory(int varsPerIndividual, int decPrecision, int representationLen) {
            NDecimalsIndividualSimpleFactory fact = new NDecimalsIndividualSimpleFactory();

            fact.setIndividualSize(varsPerIndividual);
            fact.setDecimalScale(decPrecision);
            fact.setPrecision(representationLen);
            fact.setConstraint(0, new RangeConstraint(1, 100));
            fact.setConstraint(1, new RangeConstraint(1, 100));

            return fact;
        }

    public void runAlgorithm() {
        ga = new ReusableSimpleGA(gaParameterSet);
        hook = new BetterResultHook();
        ga.addHook(hook);


    }

    public void printResults() {
        final int attempts = 1;
        GAResult[] allResults = new GAResult[attempts];
        for(int i = 0;i<attempts;i++){
            hook.resetEvaluationsCounter();
            GAResult result = ((ReusableSimpleGA) ga).exec();
            System.out.println("\nDONE.\n");
            System.out.println("Total fitness evaluations: " + hook.getFitnessEvaluations());
            allResults[i] = result;
        }

        System.out.println("\nALL DONE.\n");
        for(int i = 0;i<attempts;i++){
            System.out.println("Result " + i + " is: " + allResults[i]);
        }
    }
}
