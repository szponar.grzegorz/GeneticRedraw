package Control;

import org.jaga.definitions.SelectionAlgorithm;
import org.jaga.selection.RouletteWheelSelection;

/**
 * Created by d00gy on 22/02/2016.
 */
public class SelectionAlgorithmFactoryRouletteWheel implements SelectionAlgorithmFactory{

    public SelectionAlgorithm create(double number) {
        return new RouletteWheelSelection(number);
    }

}
