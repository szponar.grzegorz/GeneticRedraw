package Control;

import org.jaga.definitions.GAParameterSet;
import org.jaga.util.DefaultParameterSet;

/**
 * Created by d00gy on 22/02/2016.
 */
public class AlgorithmParameterSetFactoryDefault implements AlgorithmParameterSetFactory {

    public GAParameterSet create(){
        return new DefaultParameterSet();
    }

}
