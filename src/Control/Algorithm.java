package Control;


public interface Algorithm {

    void setParams();

    void runAlgorithm();

    void printResults();
}
