import Control.*;


public class Launcher {

    public static void main(String[] args) {
        Algorithm geneticAlgorithm = new AlgorithmGenetic(new AlgorithmParameterSetFactoryDefault(),
                new FitnessEvaluationAlgorithmFactoryExample(), new SelectionAlgorithmFactoryRouletteWheel());

        geneticAlgorithm.setParams();
        geneticAlgorithm.runAlgorithm();
        geneticAlgorithm.printResults();
    }
}
