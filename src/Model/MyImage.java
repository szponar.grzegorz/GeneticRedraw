package Model;

import java.awt.image.BufferedImage;

public class MyImage {
    private BufferedImage bufferedImage;

    public MyImage() {
    }

    public MyImage(BufferedImage bufferedImage) {
        this.bufferedImage = bufferedImage;
    }

    public Size getImageSize(){
        return new Size(bufferedImage.getWidth(), bufferedImage.getHeight());
    }

    public boolean isEmpty() {
        return bufferedImage == null;
    }

    public BufferedImage getImageCopy(){
        return bufferedImage.getSubimage(0,0,bufferedImage.getWidth(), bufferedImage.getHeight());
    }
}
