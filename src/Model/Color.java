package Model;

public class Color {
    private int red;
    private int green;
    private int blue;
    private int alpha;

    public Color(int red, int green, int blue, int alpha) {
        this.red = red;
        this.green = green;
        this.blue = blue;
        this.alpha = alpha;
    }

    public int getByteColor(Pixel pixel) {
        int red = pixel.getColor().red;
        int green = pixel.getColor().green;
        int blue = pixel.getColor().blue;
        int alpha = pixel.getColor().alpha;

        alpha = (alpha << 24) & 0xFF000000;
        red = (red << 16) & 0x00FF0000;
        green = (green << 8) & 0x0000FF00;
        blue = blue & 0x000000FF;

        return alpha | red | green | blue;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() { return blue; }

    public int getAlpha() {
        return alpha;
    }
}
