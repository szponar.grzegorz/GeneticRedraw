package Model;

public class Pixel {
    private Coordinate coordinate;
    private Color color;

    public Pixel(Coordinate coordinate) {
        this.coordinate = coordinate;
    }

    public Pixel(Coordinate coordinate, Color color) {
        this.coordinate = coordinate;
        this.color = color;
    }

    public void setByteColor(int byteColor) {
        int red = (byteColor & 0x00ff0000) >> 16;
        int green = (byteColor & 0x0000ff00) >> 8;
        int blue = byteColor & 0x000000ff;
        int alpha = (byteColor>>24) & 0xff;
        color = new Color(red, green, blue, alpha);
    }

    public Coordinate getCoordinate() {
        return coordinate;
    }

    public Color getColor() {
        return color;
    }

}
